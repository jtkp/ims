"""
    Python 3
    Usage: python3 client.py 2222
    coding: utf-8
    
    Author: Justin Pham (z5075823)
    Adapted starter code from Wei Song
"""
from socket import *
import sys
from threading import Thread
from time import sleep
import os
import signal

def receiver():
    global alive
    global peerUsername
    global p2pAlive
    global connected
    while alive == True:
        # receive response from the server
        # 1024 is a suggested packet size, you can specify it as 2048 or others
        data = clientSocket.recv(1024)
        receivedMessage = data.decode()

        try:
            arg, msg = receivedMessage.split('// ', 1)
        except:
            arg = receivedMessage

        # parse the message received from server and take corresponding actions
        if arg == "":
            print("===== Message from server is empty! =====")
            print(f"===== Exiting... =====\n")
            os.kill(os.getpid(), signal.SIGTERM)
        elif arg == "LOGOUT":
            if len(msg) != 0:
                print(receivedMessage)
            alive = False
            if connected == True:
                myPeerSocket.sendall(f"P2P// ===== '{peerUsername}' has disconnected their P2P session with you. =====".encode())
            sleep(0.5)
            print('===== Logging out and exiting... =====')
            clientSocket.close()
            listenerSocket.close()
            os.kill(os.getpid(), signal.SIGTERM)
        elif arg == 'BROADCAST':
            print(receivedMessage)
        elif arg == 'BLOCK':
            print(receivedMessage)
        elif arg == 'WHOELSE':
            print(receivedMessage)
        elif arg == 'WHOELSESINCE':
            print(receivedMessage)
        elif arg == 'MESSAGE':
            print(receivedMessage)
        elif arg == 'P2P':
            if msg.find('wants to start a P2P session with you.') != -1:
                print(receivedMessage + ' (accept/decline)')
                username = msg.split(' ', 1)[0][1:-1]
                peerUsername = username
            elif msg.find('has accepted the P2P request!') != -1:
                port, msg = msg.split(' ', 1)
                username, msg = msg.split(' ', 1)
                username = username[1:-1]
                print("P2P// ", msg)
                peerUsername = username
                print(f"peerUsername = {peerUsername}")
                myPeerSocket.connect(("127.0.0.1", int(port)))
                print(f"===== P2P connection established with '{username}'! =====")
                p2pAlive = True
                connected = True
                p = Thread(target=p2pReceiver)
                p.start()
            elif msg.find('has declined the P2P request.') != -1:
                print("P2P// ", msg)
                peerUsername = ''
        elif arg == 'ERROR':
            print(receivedMessage)
        else:
            print("===== Message makes no sense =====")
            
def sender():
    global alive
    global peerUsername
    global connected
    clientSocket.sendall('inbox'.encode())
    sleep(0.3)
    while alive == True:
        message = input("\n===== Waiting for input... =====\n")
        try:
            arg, msg = message.split(' ', 1)
        except:
            arg = message
            msg = ''
        if arg == "logout":
            clientSocket.sendall(message.encode())
            alive = False
            break
        elif arg == 'block':
            if len(msg) == 0:
                print('ERROR// No argument given.')
                continue
            confirm = input(f"===== Are you sure you want to block '{msg}'? (yes/no) =====\n").lower()
            if confirm == 'yes' or confirm == 'y':
                clientSocket.sendall(message.encode())
                sleep(0.5)
            else:
                continue
        elif arg == 'unblock':
            if len(msg) == 0:
                print('ERROR// No argument given.')
                continue
            confirm = input(f"===== Are you sure you want to unblock '{msg}'? (yes/no) =====\n").lower()
            if confirm == 'yes' or confirm == 'y':
                clientSocket.sendall(message.encode())
                sleep(0.5)
            else:
                continue
        elif arg == 'whoelse':
            clientSocket.sendall(message.encode())
            sleep(0.5)
        elif arg == 'whoelsesince':
            if len(msg) == 0:
                print('ERROR// No argument given.')
                continue
            clientSocket.sendall(message.encode())
            sleep(0.5)
        elif arg == 'broadcast':
            if len(msg) == 0:
                print('ERROR// No argument given.')
                continue
            clientSocket.sendall(message.encode())
        elif arg == 'message':
            if len(msg) == 0 or len(msg.split(' ', 1)) != 2:
                print('ERROR// Incorrect number of arguments.')
                continue
            clientSocket.sendall(message.encode())
            sleep(0.5)
        elif arg == 'startprivate':
            if len(msg) == 0 or len(msg.split(' ', 1)) != 1:
                print('ERROR// Incorrect number of arguments.')
                continue
            clientSocket.sendall(message.encode())
        elif arg == 'private':
            if len(msg) == 0 or len(msg.split(' ', 1)) != 2:
                print('ERROR// Incorrect number of arguments.')
                continue
            peerName = msg.split(' ')[0]
            if peerUsername == '':
                print("ERROR// You have not connected with 'startprivate'.")
            elif peerName != peerUsername:
                print('ERROR// You do not have an active P2P session with this user.')
            else:
                myPeerSocket.sendall(('P2P// ' + message).encode())
                clientSocket.sendall('update'.encode())
        elif arg == 'stopprivate':
            if len(msg) == 0 or len(msg.split(' ', 1)) != 1:
                print('ERROR// Incorrect number of arguments.')
                continue
            peerName = msg
            if peerName != peerUsername:
                print('ERROR// You do not have an active P2P session with this user.')
            else:
                myPeerSocket.sendall(f"P2P// '===== {peerUsername}' has disconnected their P2P session with you. =====".encode())
                clientSocket.sendall('update'.encode())
        elif arg == 'accept':
            message = f'acceptp2p {peerUsername} {p2pPort}'
            connected = True
            print(f"peerUsername = {peerUsername}")
            clientSocket.sendall(message.encode())
            print(f"===== P2P session established with '{peerUsername}'! =====")
        elif arg == 'decline':
            message = f'declinep2p {peerUsername}'
            peerUsername = ''
            clientSocket.sendall(message.encode())
        else:
            print('ERROR// Unrecognised command.')

def p2pReceiver():
    global p2pAlive
    global myPeerSocket
    global peerUsername
    global connected
    while p2pAlive == True:
        # receive response from the peer
        # 1024 is a suggested packet size, you can specify it as 2048 or others
        data = myPeerSocket.recv(1024)
        receivedMessage = data.decode()

        try:
            arg, msg = receivedMessage.split('// ', 1)
        except:
            arg = receivedMessage

        # parse the message received from peer and take corresponding actions
        if arg == "":
            print("ERROR// User is no longer online.")
            p2pAlive = False
            print(f"===== P2P session with '{peerUsername}' disconnected. =====")
            peerUsername = ''
            connected = False
            myPeerSocket = socket(AF_INET, SOCK_STREAM)
        elif arg == 'P2P':
            if msg.find('has disconnected their P2P session with you.') != -1:
                p2pAlive = False
                connected = False
                peerUsername = ''
                print(msg)
                myPeerSocket.sendall('P2P// Confirming disconnect.'.encode())
                myPeerSocket = socket(AF_INET, SOCK_STREAM)
            elif msg.find('Confirming disconnect.') != -1:
                p2pAlive = False
                connected = False
                print(f"===== P2P session with '{peerUsername}' disconnected. =====")
                peerUsername = ''
                myPeerSocket.close()
                myPeerSocket = socket(AF_INET, SOCK_STREAM)
            else:
                msg = msg.split(' ', 2)[2]
                print(f'P2P// {peerUsername}: {msg}')
        elif arg == 'ERROR':
            print(receivedMessage)
        else:
            print("===== Message makes no sense =====")

def process_login():
    print("==============================================")
    print("===== Welcome to Justin's messaging app! =====")
    print("==============================================\n")
    message = input("===== Please enter your username: =====\n")
    clientSocket.sendall((message).encode())

    while True:
        data = clientSocket.recv(1024)
        receivedMessage = data.decode()

        if receivedMessage == 'found':
            print("===== User found, enter password: =====")
            clientSocket.sendall(input().encode())
        elif receivedMessage == 'not found':
            print("===== User not found, creating a new account... =====")
            print("===== Enter a new password: =====")
            clientSocket.sendall(input().encode())
        elif receivedMessage == 'password incorrect':
            print("===== Password incorrect, try again: =====")
            clientSocket.sendall(input().encode())
        elif receivedMessage == 'already online':
            print("===== User is already online. =====")
            print("===== Please enter your username: =====")
            clientSocket.sendall(input().encode())
        elif receivedMessage == 'still blocked':
            print("===== You are still blocked from logging in. =====")
            clientSocket.sendall(input().encode())
        elif receivedMessage == 'account created':
            print("===== Account successfully created! =====")
            print("===== Login successful! =====")
            return
        elif receivedMessage == 'logged in':
            print("===== Login successful! =====")
            return
        else:
            print("===== You have failed to enter the correct password too many times... =====")
            print(f"===== You have been blocked for {receivedMessage} seconds! =====")
            print(f"===== Exiting... =====\n")
            exit(0)


if __name__ == "__main__":
    # Server would be running on the same host as Client
    if len(sys.argv) != 2:
        print("\n===== ERROR: usage, python3 client.py SERVER_PORT ======\n")
        exit(0)
    serverHost = "127.0.0.1"
    serverPort = int(sys.argv[1])
    serverAddress = (serverHost, serverPort)

    # define a socket for the client side, it would be used to communicate with the server
    clientSocket = socket(AF_INET, SOCK_STREAM)

    # initialise p2p sockets
    listenerSocket = socket(AF_INET, SOCK_STREAM)
    listenerSocket.bind(('127.0.0.1', 0))
    p2pPort = listenerSocket.getsockname()[1]  
    myPeerSocket = socket(AF_INET, SOCK_STREAM)

    # build connection with the server and send message to it
    clientSocket.connect(serverAddress)

    process_login()

    peerAddress = ''
    peerUsername = ''
    alive = True
    p2pAlive = True
    connected = False

    t = Thread(target=receiver, daemon=True)
    t.start()
    s = Thread(target=sender, daemon=True)
    s.start()


    while alive == True:
        listenerSocket.listen()
        myPeerSocket, peerAddress = listenerSocket.accept()
        p2pAlive = True
        p = Thread(target=p2pReceiver, daemon=True)
        p.start()

    clientSocket.close()
