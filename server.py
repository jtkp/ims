"""
    Multi-Threaded Server
    Python 3
    Usage: python3 server.py 2222 10 10
    coding: utf-8
    
    Author: Justin Pham (z5075823)
    Adapted starter code from Wei Song
"""
from pathlib import Path
from socket import *
from threading import Thread, Lock
from time import time, sleep
import sys, select

# SERVER ADMIN
def new_user(username, password):
    with c_lock:
        open('credentials.txt', 'a').write(f'\n{username} {password}')
        user_info = {
            'username': username,
            'password': password,
            'blocked': time(),
            'last_online': time(),
            'messages': []
        }
        credentials[username] = user_info
        blocked[username] = []

def is_online(username):
    for c in clients:
        if c['user']['username'] == username:
            return True
    return False

def is_blocked(username):
    if credentials[username]['blocked'] >= time():
        return True
    return False

def user_exists(username):
    if username in credentials:
        return True
    return False
    
def user_blocked(sender, username):
    if username in blocked[sender]:
        return True
    return False

def check_timeout():
    while True:
        for i, t in enumerate(threads):
            if t.user != '' and t.last_command <= (time() - timeoutInterval):
                with thread_lock:
                    sock = t.clientSocket
                    sock.sendall('LOGOUT// Timeout due to inactivity, please log in again.'.encode())
                    threads.pop(i)
        sleep(1)

def broadcast(msg, sender):
    for c in clients:
        if user_blocked(sender, c['user']['username']):
            continue
        message = f'BROADCAST// {msg}'
        c['socket'].sendall(message.encode())

"""
    Define multi-thread class for client
    This class would be used to define the instance for each connection from each client
    For example, client-1 makes a connection request to the server, the server will call
    class (ClientThread) to define a thread for client-1, and when client-2 make a connection
    request to the server, the server will call class (ClientThread) again and create a thread
    for client-2. Each client will be running in a separate thread, which is the multi-threading
"""
class ClientThread(Thread):
    def __init__(self, clientAddress, clientSocket):
        Thread.__init__(self)
        self.clientAddress = clientAddress
        self.clientSocket = clientSocket
        self.clientAlive = False
        self.user = ''
        self.last_command = time()
        
        self.clientAlive = True

    def disconnect(self):
        # NEED a lock here
        name = ''
        for i, c in enumerate(clients):
            if c['socket'] == self.clientSocket:
                name = clients.pop(i)['user']['username']
                break

        broadcast(f"'{name}' is now offline!", name)
        self.clientAlive = False
        message = 'LOGOUT// '
        self.clientSocket.sendall(message.encode())
        
    def update_last_command(self):
        self.last_command = time()
    
    def process_login(self):
        while self.clientAlive:
            # use recv() to receive message from the client
            data = self.clientSocket.recv(1024)
            username = data.decode()

            if username == '':
                self.disconnect()
                return

            if username in credentials:
                if is_online(username):
                    message = 'already online'
        
                    self.clientSocket.sendall(message.encode())
                    continue
                elif is_blocked(username):
                    message = 'still blocked'
        
                    self.clientSocket.sendall(message.encode())
                    continue
                self.found_user(username)
                return
            else:
                self.create_user(username)
                return
                
    def found_user(self, username):
        user = credentials[username]

        message = 'found'
        self.clientSocket.sendall(message.encode())

        count = 0
        while self.clientAlive:
            data = self.clientSocket.recv(1024)
            password = data.decode()

            if password == '':
                self.disconnect()
                return

            if password == user['password']:
                
                credentials[username]['last_online'] = time()
                broadcast(f"'{username}' is now online!", username)
                c = {
                    'socket': self.clientSocket,
                    'user': credentials[username]
                }
                clients.append(c)
                self.user = credentials[username]

                message = 'logged in'
    
                self.clientSocket.sendall(message.encode())
                self.update_last_command()

                return
            elif count < 2:
                count += 1
                message = 'password incorrect'
    
                self.clientSocket.sendall(message.encode())
            else:
                message = str(blockDuration)
                user['blocked'] = time() + blockDuration
    
                self.clientSocket.sendall(message.encode())
                return
 
    def create_user(self, username):
        message = 'not found'
        self.clientSocket.sendall(message.encode())

        data = self.clientSocket.recv(1024)
        password = data.decode()
        if password == '':
            self.disconnect()
            return

        new_user(username, password)

        credentials[username]['last_online'] = time()
        broadcast(f'{username} is now online!', username)
        c = {
            'socket': self.clientSocket,
            'user': credentials[username]
        }
        clients.append(c)
        self.user = credentials[username]
        blocked[username] = []

        message = 'account created'
        self.clientSocket.sendall(message.encode())
        self.update_last_command()

    def process_block(self, username):
        user = self.user['username']
        message = ''

        if not user_exists(username) or user == username:
            message = f'BLOCK// Invalid user.'
        elif username in blocked[user]:
            message = f"BLOCK// '{username}' is already blocked."
        else:
            with b_lock:
                blocked[user].append(username)
            message = f"BLOCK// '{username}' has been blocked."

        self.clientSocket.sendall(message.encode())
        self.update_last_command()

    def process_unblock(self, username):
        user = self.user['username']
        message = ''

        if not user_exists(username) or user == username:
            message = f'BLOCK// Invalid user.'
        elif username not in blocked[user]:
            message = f"BLOCK// '{username}' is not currently blocked."
        else:
            with b_lock:
                blocked[user].remove(username)
            message = f"BLOCK// '{username}' has been unblocked."

        self.clientSocket.sendall(message.encode())
        self.update_last_command()

    def process_whoelse(self):
        alone = True
        for c in clients:
            username = c['user']['username']
            if username != self.user['username'] and self.user['username'] not in blocked[username]:
                alone = False
                message = f"WHOELSE// {username}"
                self.clientSocket.sendall(message.encode())
        if alone == True:
            message = f"WHOELSE// Noone else is online."
            self.clientSocket.sendall(message.encode())
        self.update_last_command()
                
    def process_whoelsesince(self, since):
        alone = True
        for key, value in credentials.items():
            username = value['username']
            if (username != self.user['username']
                and self.user['username'] not in blocked[username]
                and value['last_online'] >= (time() - since)):
                alone = False
                message = f"WHOELSESINCE// {username}"
                self.clientSocket.sendall(message.encode())
        if alone == True:
            message = f"WHOELSESINCE// Noone else has logged on since that time."
            self.clientSocket.sendall(message.encode())
        self.update_last_command()
    
    def process_broadcast(self, msg):
        for c in clients:
            if (user_blocked(self.user['username'], c['user']['username'])
                or c['user']['username'] == self.user['username']):
                continue
            message = f"BROADCAST// {self.user['username']}: {msg}"

            c['socket'].sendall(message.encode())
        self.update_last_command()

    def process_message(self, msg):
        # check receiver
        receiver, msg = msg.split(' ', 1)

        if receiver not in credentials or receiver == self.user['username']:
            message = f"ERROR// Invalid user."

            self.clientSocket.sendall(message.encode())
        elif self.user['username'] in blocked[receiver]:
            message = f"ERROR// You have been blocked by '{receiver}'."

            self.clientSocket.sendall(message.encode())
        else:
            message = f"MESSAGE// {self.user['username']}: {msg}"
            for c in clients:
                if receiver == c['user']['username']:
                    c['socket'].sendall(message.encode())
                    self.update_last_command()
                    return
            with c_lock:
                credentials[receiver]['messages'].append(message + '\n')
        self.update_last_command()
        
    def process_inbox(self):
        if len(self.user['messages']) != 0:
            # acq lock
            for msg in self.user['messages']:
                self.clientSocket.sendall(msg.encode())
            self.user['messages'] = []
            with c_lock:
                credentials[self.user['username']]['messages'] = []
            
    def process_startprivate(self, username):
        if username == self.user['username']:
            message = 'ERROR// Cannot start a private session with yourself.'
            self.clientSocket.sendall(message.encode())
            self.update_last_command()
            return
        elif username not in credentials:
            message = 'ERROR// Invalid user.'
            self.clientSocket.sendall(message.encode())
            self.update_last_command()
            return
        elif self.user['username'] in blocked[username]:
            message = f"ERROR// You have been blocked by '{username}'."

            self.clientSocket.sendall(message.encode())
            self.update_last_command()
            return
        for c in clients:
            if c['user']['username'] == username:
                message = f"P2P// '{self.user['username']}' wants to start a P2P session with you."
                c['socket'].sendall(message.encode())
                self.update_last_command()
                return
        message = 'ERROR// User is offline.'
        self.clientSocket.sendall(message.encode())
        self.update_last_command()
                    
    def process_declinep2p(self, username):
        for c in clients:
            if c['user']['username'] == username:
                message = f"P2P// '{self.user['username']}' has declined the P2P request."
                c['socket'].sendall(message.encode())
                self.update_last_command()
                return
        message = f"ERROR// '{username}' is no longer online."
        self.clientSocket.sendall(message.encode())
        self.update_last_command()
                    
    def process_acceptp2p(self, username):
        username, peerPort = username.split(' ', 1)
        for c in clients:
            if c['user']['username'] == username:
                message = f"P2P// {peerPort} '{self.user['username']}' has accepted the P2P request!"
                c['socket'].sendall(message.encode())
                self.update_last_command()
                return
        message = f"ERROR// '{username}' is no longer online."
        self.clientSocket.sendall(message.encode())
        self.update_last_command()


    def run(self):
        message = ''
        
        self.process_login()

        # if enters here, client has successfully logged in
        while self.clientAlive:
            # use recv() to receive message from the client
            data = self.clientSocket.recv(1024)
            message = data.decode()
            try:
                arg, msg = message.split(' ', 1)
            except:
                arg = message
            
            # if the message from client is empty, the client would be offline then set the client as offline (alive=False)
            if arg == '':
                self.disconnect()
                break
            
            # handle message from the client
            if arg == 'logout':
                self.disconnect()
                break
            elif arg == 'block':
                self.process_block(msg)
            elif arg == 'unblock':
                self.process_unblock(msg)
            elif arg == 'whoelse':
                self.process_whoelse()
            elif arg == 'whoelsesince':
                self.process_whoelsesince(int(msg))
            elif arg == 'broadcast':
                self.process_broadcast(msg)
            elif arg == 'message':
                self.process_message(msg)
            elif arg == 'inbox':
                self.process_inbox()
            elif arg == 'startprivate':
                self.process_startprivate(msg)
            elif arg == 'declinep2p':
                self.process_declinep2p(msg)
            elif arg == 'acceptp2p':
                self.process_acceptp2p(msg)
            elif arg == 'update':
                self.update_last_command()


if __name__ == "__main__":
    # acquire server host and port from command line parameter
    if len(sys.argv) != 4:
        exit(0)
    serverHost = "127.0.0.1"
    serverPort = int(sys.argv[1])
    blockDuration = int(sys.argv[2])
    timeoutInterval = int(sys.argv[3])
    serverAddress = (serverHost, serverPort)

    # define socket for the server side and bind address
    serverSocket = socket(AF_INET, SOCK_STREAM)
    serverSocket.bind(serverAddress)

    # data structures
    thread_lock = Lock()
    threads = []
    clients = []
    b_lock = Lock()
    blocked = {}
    c_lock = Lock()
    credentials = {}

    # read credentials.txt and set up initial info
    login_info = Path('credentials.txt').read_text().split('\n')
    for user in login_info:
        username = user.split(' ')[0]
        password = user.split(' ')[1]
        user_info = {
            'username': username,
            'password': password,
            'blocked': 0,
            'last_online': 0,
            'messages': []
        }
        credentials[username] = user_info
        blocked[username] = []

    t = Thread(target=check_timeout)
    t.start()
    
    while True:
        serverSocket.listen()
        clientSockt, clientAddress = serverSocket.accept()
        clientThread = ClientThread(clientAddress, clientSockt)
        clientThread.start()
        threads.append(clientThread)
